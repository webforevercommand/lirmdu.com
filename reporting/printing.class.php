<?php

class Printing extends tFPDF
{
    function Title($title, $image, $company_name, $company_adres, $company_tel, $company_site)
    {
        $this->Image($image, 6, 6, 30, 20);
        $this->Cell(30);
        $this->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
        $this->SetFont('DejaVu', '', 10);
        $this->Cell(40, 4, $company_name, 0, 0, 'L', 0);
        $this->Cell(70);
        $this->SetFillColor(187, 189, 189);
        $this->Cell(50, 4, $title, 0, 0, 'C', 1);
        $this->ln();
        $this->Cell(30);
        $this->Cell(40, 4, $company_adres, 0, 10, 'L', 0);
        $this->Cell(40, 4, $company_tel, 0, 10, 'L', 0);
        $this->Cell(40, 4, $company_site, 0, 10, 'L', 0);
        $this->ln();
        $this->ln();
    }


}

?>