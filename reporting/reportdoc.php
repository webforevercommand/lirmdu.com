<?php
require 'vendor/autoload.php';
$id = $_GET['id'];
$phpWord = new  \PhpOffice\PhpWord\PhpWord();

$phpWord->setDefaultFontName('Times New Roman');
$phpWord->setDefaultFontSize(14);

$properties = $phpWord->getDocInfo();

$search = $_SESSION['search'];

$sectionStyle = array(
    'portrait' => 'landscape',
    'marginTop' => \PhpOffice\PhpWord\Shared\Converter::pixelToTwip(10),
);
$section = $phpWord->addSection($sectionStyle);
$section->addImage('logo.jpg',
    array('width' => 200,
        'height' => 200)
);
$fontStyle = array('name' => 'Times New Roman', 'size' => 16,'color' => '075776','italic'=>true);
$listStyle = array('listType'=>\PhpOffice\PhpWord\Style\ListItem::TYPE_NUMBER);


$fontStyle = array('name' => 'Times New Roman', 'size' => 24,'italic'=>true);
$phpWord->addTitleStyle(6,$fontStyle);
$section->addTitle('Вас вітає команда розробників 22 кафедри',6);



include('../public/connect.php');
$connect = connectDB();
$sql = "SELECT * FROM infores WHERE id_ir = $id";
$text = '';
$result = $connect->query($sql);
while ($row = $result->fetch_array()) {
    $text .= $row['title'];
    $text1 .= 'Доменне імя: ' . $row['www_ir'];
    $text2 .= 'Дата реєстрації: ' . $row['datare'];
    $text3 .= $row['owners'];
    $text4 .= "";
    $section->addText(htmlspecialchars($text),
        array('name' => 'Times New Roman', 'size' => 14),
        array('spaceBefore' => 10)
    );
    $section->addText(htmlspecialchars($text1),
        array('name' => 'Times New Roman', 'size' => 14),
        array('spaceBefore' => 10)
    );
    $section->addText(htmlspecialchars($text2),
        array('name' => 'Times New Roman', 'size' => 14),
        array('spaceBefore' => 10)
    );
    $section->addText(htmlspecialchars($text3),
        array('name' => 'Times New Roman', 'size' => 14),
        array('spaceBefore' => 10)
    );
    $section->addText(htmlspecialchars($text4),
        array('name' => 'Times New Roman', 'size' => 14),
        array('spaceBefore' => 10)
    );
    $text = "";
    $text1 = "";
    $text2 = "";
    $text3 = "";
}
closeDB($connect);


header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="lirmdu.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$xmlWriter->save("php://output");
