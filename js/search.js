$(".search_button").click(function () {
    changePage(0);
});
function changePage(page) {
    var search = $("#search").val();
    var checkedValues = $('#myForm input:checkbox:checked').map(function () {
        return this.value;
    }).get();
    $.ajax({
        type: "POST",
        url: "search.php",
        data: {
            "search": search,
            "page": page,
            "checkedValues": checkedValues
        },
        cache: false,
        success: function (response) {
            $("#resSearch").html(response);
            $("#text").html(response);
        }
    });
    return false;
}


