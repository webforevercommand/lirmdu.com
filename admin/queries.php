<?php 
try{
	$pdo=new PDO ('mysql:host=localhost;dbname=lirmdunew','root','');
} catch(PDOException $e){
	exit($e ->getMessage());
}

//ЗАПИТИ ДЛЯ НАЛАШТУВАННЯ КАТЕГОРІЙ
const SQL_INSERT_CATEGORY='
 INSERT INTO categories(title,alias)
 VALUES      (:title,:alias)';
const SQL_UPDATE_CATEGORY_BY_ID =
'UPDATE categories SET 
   title     = :title,
   alias     = :alias
   WHERE
   id         = :id';
const SQL_GET_CATEGORY ='  SELECT *   FROM categories';
const SQL_DELETE_CATEGORY_BY_ID ='
DELETE FROM categories WHERE id = :id ';


//ЗАПИТИ ДЛЯ НАЛАШТУВАННЯ НОВИН
const SQL_INSERT_NEWS='
 INSERT INTO news (title,news)
 VALUES      (:title,:news)';

const SQL_INSERT_CATEGORY='
 INSERT INTO categories(title,alias)
 VALUES      (:title,:alias)';


const SQL_UPDATE_NEWS_BY_ID =
'UPDATE news SET 
   title     = :title,
   date_news= :date_news,
   news      = :news
   WHERE
   id_news         = :id_news';
const SQL_GET_NEWS ='  SELECT *   FROM news';
const SQL_DELETE_NEWS_BY_ID ='
DELETE FROM news WHERE id_news = :id_news ';

//ЗАПИТИ ДЛЯ НАЛАШТУВАННЯ ІНФОРМАЦІЙНИХ РЕСУРСІВ
const SQL_INSERT_INFORES='
 INSERT INTO 
 infores(
key_words,
datare,
mithe_znahod,
title,
owner_ir,
www_ir,
phone_number,
title_classif,
owners,
region,
email_ir,
description_ir
 )
 VALUES (
:key_words,
:datare,
:mithe_znahod,
:title,
:owner_ir,
:www_ir,
:phone_number,
:title_classif,
:owners,
:region,
:email_ir,
:description_ir
)';
const SQL_UPDATE_INFORES_BY_ID ='
UPDATE  infores SET
key_words      =:key_words,
datare         =:datare,
mithe_znahod   =:mithe_znahod,
title          =:title,
owner_ir       =:owner_ir,
www_ir         =:www_ir,
phone_number   =:phone_number,
title_classif  =:title_classif,
owners         =:owners,
region         =:region,
email_ir       =:email_ir,
description_ir =:description_ir
WHERE 
id_ir =:id_ir';
const SQL_GET_INFORES ='
SELECT * FROM infores';



const SQL_DELETE_INFORES_BY_ID ='
DELETE FROM infores WHERE id_ir =:id_ir';
//ЗАПИТИ ДЛЯ НАЛАШТУВАННЯ АДМІНІСТРАТОРІВ
const SQL_UPDATE_USER_BY_ID ='
UPDATE user SET 
login          =:login,
password       =:password,
name           =:name,
surname        =:surname,
email          =:email,
tel            =:tel,
law            =:law
WHERE
id_user        =:id_user';
const SQL_GET_USER ='SELECT * FROM user';
const SQL_INSERT_USER='
 INSERT INTO
 user(login,
password,
name,
surname,
email,
tel,
law) 
VALUES(
:login,
:password,
:name,
:surname,
:email,
:tel,
:law) ';
const SQL_DELETE_USER_BY_ID ='
DELETE FROM user WHERE id_user =:id_user';


//CONTACT MESSAGE
const SQL_GET_CONTACT ='SELECT * FROM contact';

const SQL_DELETE_MESSAGE_BY_ID =' DELETE FROM contact WHERE id_contact =:id_contact';