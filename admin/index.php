<?php include "header.php"; ?>

<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Головна</a>
        </li>
        
      </ol>
 <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-6 col-sm-6 mb-3">
          <div class="card text-white bg-primary o-hidden ">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-comments"></i>
              </div>
             <h1><div class="mr-5">            <?php   $message=0;
            $res=$pdo->query(SQL_GET_CONTACT);
            foreach ($res as $row) {$message++;}
              if ($message > 0) {echo '+'.$message; }?>  Нових повідомлень!</div></h1>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="message.php">
              <span class="float-left">Переглянути</span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <style>.content.projects .box {
  margin-bottom: 25px;
}

.emp_box .box {
  margin-bottom: 25px;
}

.page1_block {
  margin-bottom: 20px;
}

.page1_block .box h3 {
  margin-bottom: 30px;
}

.page1_block .box {
  margin-bottom: 25px;
}
.page1_block {
  margin-bottom: 20px;
}
.page1_block .box h3 {
  margin-bottom: 30px;
}
.page1_block .box {
  margin-bottom: 25px;
}
.page1_block p{font-size: 14px;}</style>
        <div class="col-xl-6 col-sm-6 mb-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-list"></i>
              </div>
           <h1>   <div class="mr-5" style='color:black;'>Незареєстрованих ресурсів!</div>
            </div></h1>
            <a class="card-footer text-white clearfix small z-1" href="resourse_edit_advence.php">
              <span class="float-left">Переглянути</span>
              <span class="float-right">
                <i class="Sfa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
      </div>
<div class="page1_block">
  <div class="container_12">
    <div class="grid_3">
      <div class="box ic1 "><div class="maxheight">
        <h3>Повідомлення </h3> <i class="fa fa-envelope-open-o" aria-hidden="true"></i></i>
          <p>Розділ містить повідомлення надісланні користувачами сайту  <br>  </p>
        <a href="message.php" class="btn">Переглянути</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic2"><div class="maxheight">
        <h3>Новини</h3><i class="fa fa-newspaper-o" aria-hidden="true"></i><p>Маєте можливість додати, редагувати або видалити новину на сайті</p>
        <a href="address.php" class="btn">Переглянути</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic3"><div class="maxheight">
        <h3>Категорії</h3><i class="fa fa-map-signs" aria-hidden="true"></i>
        <p>Керування категоріями які допомагають користувачам у пошуку контенту</p>
        <a href="phone.php" class="btn">Переглянути</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic4"><div class="maxheight">
        <h3>Реєстр ресурсів</h3> <i class="fa fa-database" aria-hidden="true"></i><p>Розділ призначений для керування інформаційними ресурсами</p>
        <a href="infosystem.php" class="btn">Переглянути</a></div>
      </div>
    </div>
        <div class="grid_3">
      <div class="box ic1 "><div class="maxheight">
        <h3>Адміністратори </h3> <i class="fa fa-user-secret" aria-hidden="true"></i>
          <p>Розділ призначений для керування користувачами інформаційного ресурсу </p>
        <a href="message.php" class="btn">Переглянути</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic2"><div class="maxheight">
        <h3>База даних</h3> <i class="fa fa-arrow-circle-down" aria-hidden="true"></i><p>Натиснувши кнопку ви можете отримати дамп Бази даних.</p>
        <a href="../damp.php" class="btn">Переглянути</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic3"><div class="maxheight">
        <h3>Статистика</h3> <i class="fa fa-bar-chart" aria-hidden="true"></i>
        <p>Керування категоріями які допомагають користувачам у пошуку контенту</p>
        <a href="phone.php" class="btn">Переглянути</a></div>
      </div>
    </div>
    <div class="grid_3">
      <div class="box ic4"><div class="maxheight">
        <h3>Налаштування</h3>  <i class="fa fa-cogs" aria-hidden="true"></i><p>Розділ містить налаштування сайту та адміністративної панелі</p>
        <a href="../damp.php" class="btn">Переглянути</a></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php include "footer.php"; ?>
