<?php include "queries.php"; ?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Інформаційний ресурс </title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet"> 
  <link href="css/styleForm.css" rel="stylesheet"> 
  <link href="css/skeleton.css" rel="stylesheet">
  
  <link  rel="stylesheet" media="screen" href="csss/style.css">
  <script type="text/javascript" src="htmledit/ckeditor/ckeditor.js"></script>


    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Спойлеры</title>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    
    
<script type="text/javascript">
$(document).ready(function(){
 $('.spoiler_links').click(function(){
  $(this).parent().children('div.spoiler_body').toggle('normal');
  return false;
 });
});
</script>
    
<style type="text/css">
 .spoiler_body {display:none;}
 .spoiler_links {cursor:pointer;}
</style>
    

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" >

<!-- Навігація-->

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"  id="mainNav">
    <a class="navbar-brand" href="index.php">Адміністративна панель</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <!-- Головна-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="index.php">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span class="nav-link-text">Головна</span>
          </a>
        </li>
    <!-- Відкрити сайт-->
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a  class="nav-link" href="../index.php">
            <i class="fa fa-desktop" aria-hidden="true"></i>
            <span class="nav-link-text">Відкрити сайт</span>
          </a>
        </li> 
     <!--Повідомлення-->
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a  href="message.php" class="nav-link" href="#">
           <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
            <span class="nav-link-text">Повідомлення <div style="float: right; color: white;">           
            <?php   $message=0;
            $res=$pdo->query(SQL_GET_CONTACT);
            foreach ($res as $row) {$message++;}
              if ($message > 0) {echo '+'.$message; }?>  </div></span>
          </a>
        </li>
     <!-- Сторінки-->
 <!--         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-sitemap"></i>
            <span class="nav-link-text">Сторінки</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
       <a href="page_add.php"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Додати </a>
            </li>
            <li>
              <a href="page_edit.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редагувати</a>
            </li>
          </ul>
        </li>   -->
           <!-- Новини-->
     <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#news" data-parent="#exampleAccordion">
           <i class="fa fa-newspaper-o" aria-hidden="true"></i>
            <span class="nav-link-text">Новини</span>
          </a>
          <ul class="sidenav-second-level collapse" id="news">
           <li>
       <a href="news_add.php"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Додати </a>
            </li>
            <li>
              <a href="news_edit.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редагувати</a>
            </li>
          </ul>
        </li>  
                       <!-- Категорії-->
     <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#category" data-parent="#exampleAccordion">
            <i class="fa fa-map-signs" aria-hidden="true"></i>
            <span class="nav-link-text">Категорії</span>
          </a>
          <ul class="sidenav-second-level collapse" id="category">
           <li>
       <a href="category_add.php"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Додати </a>
            </li>
            <li>
              <a href="category_edit.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редагувати</a>
            </li>
          </ul>
        </li> 
          <!-- Повідомлення-->

                    
       <!-- Реєстр ресурсів-->
     <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-database" aria-hidden="true"></i>
            <span class="nav-link-text">Реєстр ресурсів </span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
           <li>
       <a href="resourse_add.php"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Додати </a>
            </li>
            <li>
              <a href="resourse_edit.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редагувати</a>
            </li>
            <li>
              <a href="resourse_edit_advence.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Непідтвердженні ресурси <div style="float: right; color: white;">


             </div> </a>
            </li>
          </ul>
        </li>

    <!-- Адміністратори-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMultiii" data-parent="#exampleAccordion">
            <i class="fa fa-user-secret" aria-hidden="true"></i>
            <span class="nav-link-text">Адміністратори</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMultiii">
            <li>
       <a href="admin_add.php"> <i class="fa fa-user-plus" aria-hidden="true"></i> Додати </a>
            </li>
            <li>
              <a href="admin_edit.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Редагувати</a>
            </li>
          </ul>
        </li>
    
     <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href=".php">
           <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
            <span class="nav-link-text">База даних</span>
          </a>
        </li>
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link"href="stat.php">
            <i class="fa fa-bar-chart" aria-hidden="true"></i>
            <span class="nav-link-text">Статистика відвідувань</span>
          </a>
        </li>
     <!-- Налаштування дизайну-->
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#settingdesign" data-parent="#exampleAccordion">
            <i class="fa fa-user-secret" aria-hidden="true"></i>
            <span class="nav-link-text">Налаштування інтерфейсу</span>
          </a>
          <ul class="sidenav-second-level collapse" id="settingdesign">
            <li>
       <a href="admin_add.php"> <i class="fa fa-user-plus" aria-hidden="true"></i> Заголовок </a>
            </li>
            <li>
              <a href="admin_edit.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Лівий блок</a>
            </li>
            <li>
              <a href="admin_del.php"><i class="fa fa-user-times" aria-hidden="true"></i>Меню</a>
            </li>
            <li>
              <a href="admin_del.php"><i class="fa fa-user-times" aria-hidden="true"></i>Робоча область</a>
            </li>
            <li>
              <a href="admin_del.php"><i class="fa fa-user-times" aria-hidden="true"></i> Підвал</a>
            </li>
          </ul>
        </li>
    <!-- Налаштування-->
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a  href="setting.php" class="nav-link" href="#">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="nav-link-text">Налаштування</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
    <!-- Нові повідомлення-->
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Повідомлення
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">Нове повідомлення</h6>
            <a class="dropdown-item small" href="#">Подивитись всі повідомлення</a>
          </div>
        </li>
        
       
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Вихід</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container-fluid">
     

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ви дійсно хочете вийти?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Натисніть кнопку "Вихід" для того щоб закінчити сеанс.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Закрити</button>
            <a class="btn btn-primary" href="login.html">Вихід</a>
          </div>
        </div>
      </div>
    </div>
