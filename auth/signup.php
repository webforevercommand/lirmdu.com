<?php 
	session_start();
	ob_start();
    date_default_timezone_set('Europe/Kiev');
	require "includes/connection.php";
	include("send.php");
	include("includes/header.php");
	$data = $_POST;
	if (isset($data["signup"])) {
		$errors = array();
		if ($data["surname"] == "") {
			$errors[] = "Введіть прізвище";
		}
		if ($data["name"] == "") {
			$errors[] = "Введіть імя";
		}
		if ($data["email"] == "") {
			$errors[] = "Введіть email";
		}
		if ($data["tel"] == "") {
			$errors[] = "Введіть телефон";
		}
		if (strlen($data["tel"]) != 13) {
			$errors[] = "Введіть номер телефону згідно зразку";
		}
		if ($data["username"] == "") {
			$errors[] = "Введіть логін";
		}
		if (R::count("user", "login = ?", array($data["username"]))) {
			$errors[] = "Користувач з таким логіном вже існує";
		}
		if ($data["password"] == "") {
			$errors[] = "Введіть пароль";
		}
		if ($data["password"] != $data["password_2"]) {
			$errors[] = "Паролі не співпадають";
		}
		if ($data["capcha"] == "") {
			$errors[] = "Введіть капчу ";
		}
		if (md5($data["capcha"]) != $_SESSION['randomnr2']) {
			$errors[] = "Введіть капчу повторно";
		}
		if (empty($errors)) {
			$user = R::dispense("user");
			$user->login = $data["username"];
			$user->password = hash("sha256", $data["password"]);
			$user->name = $data["name"];
			$user->surname = $data["surname"];
			$user->email = $data["email"];
			$user->tel = $data["tel"];
			$user->reg_time = date(DATE_RSS);
			R::store($user);
			if(Send() == 1) {
				echo "<div class=\"error\">На ваш почтовий адрес відправлено повідомлення с підтвердженям</div>";
			} else{
				echo "<div class=\"error\">Повідомлення на почтовий адрес не відправлено. Сбробуйте пізніше!!!</div>";
			}
			unset($data);
		} else {
            echo "<div class=\"error\">" . "ПОВІДОМЛЕННЯ: ". array_shift($errors) . "</div>";
		}
	}
?>
    <div id="container" class="container mregister">
    <div id="login">
	<h1>РЕЄСТРАЦІЯ</h1>
<form name="registerform" action="signup.php" method="post">
	<p>
		<label for="surname">Прізвище<br >
		<input class="input" type="text" name="surname" id="surname" size="32" value="" autocomplete="off" onchange="surnameValid();">
            <div id="surname-error" class="error-text" style="display:none">Заборонено цифри та інші символи</div>
        </label>

    </p>

    <p>
		<label for="name">Імя<br>
		<input class="input" type="text" name="name" id="name" size="32" value="" autocomplete="off" onchange="nameValid();">
            <div id="name-error" class="error-text" style="display:none">Заборонено цифри та інші символи</div>
        </label>
    </p>
	
	<p>
		<label for="email">Email<br>
		<input class="input" type="email" name="email" id="email" value="" size="32" autocomplete="off" onchange="emailValid()">
            <div id="email-error" class="error-text" hidden="false">Неправильна форма почтової скриньки</div>
            <div id="email-error-ru" class="error-text" hidden="false">Заборонено використовувати почтові скриньки з ru</div>
        </label>
	</p>
	<p>
		<label for="tel">Номер телефону<br>
		<input class="input" type="text" name="tel" id="tel" value="" size="32" autocomplete="off" onchange="telValid()">
            <div id="tel-error" class="error-text" hidden="false">Неправильна форма номеру телефона:<br>Наприклад:+380661464576</div>
        </label>
	</p>
	
	<p>
		<label for="username">Логін<br>
		<input class="input" type="text" name="username" id="username" value="" size="20" autocomplete="off" onchange="loginValid()">
            <div id="username-error" class="error-text" style="display:none">Логін повинен бути більше 3 символів</div>
        </label>
	</p>
	
	<p>
		<label for="password">Пароль<br>
		<input class="input" type="password" name="password" id="password" value="" size="32" autocomplete="off">
        </label>
	</p>	
	<p>
		<label for="password_2">Введіть пароль повторно<br />
		<input class="input" type="password" name="password_2" id="password_2" value="" size="32" autocomplete="off" onchange="passValid();">
            <div id="password-error" class="error-text" style="display:none">Паролі не співпадають</div>
        </label>
	</p>
	<p>
		<img src = "captcha.php" alt = "Каптча"><br>
		<label for="capcha">Введіть текст із зображенння
  		<input class="input" type = "text" name = "capcha" value = "" id = "capcha" size = "10" autocomplete="off" onkeyup="capchaValid()">
            <div id="capcha-error" class="error-text" style="display:none">Можуть бути тільки цифри</div>
        </label>
    </p>
	<p>
		<input id="button-dis" class="button" type="submit" name="signup" value="ЗАРЕГІСТРУВАТИСЯ" onclick="progresBar();" disabled>
	</p>
    <p>
        <input id="button-valid" class="button" type="submit" name="signup" value="ЗАРЕГІСТРУВАТИСЯ" onclick="progresBar();" style="display: none">
    </p>

	<p>Ви вже зареєстровані? <br><a href="login.php" >Вхід тут</a>!</p>
    <p style="float: right;"><a href="/index.php">На головну</a></p>
</form>

    </div>

    </div>
    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <p>На вашу почтову скриньку було відправленно повідомлення з підтвердженням</p>
        </div>

    </div>

    <div id="myLoader" style="display: none;">
        <div class="loader"></div>
    </div>

<?php 
	ob_flush();
    include("includes/footer.php");
?>