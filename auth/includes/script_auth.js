function surnameValid() {
    var reg = new RegExp("^.*[^A-zА-яЁё].*$");
    var val = document.getElementById("surname");
    var textError = document.getElementById("surname-error");
    if (!reg.test(val.value) && (val.value.length > 2)) {
        val.className = "valid-input";
        textError.style.display = "none";
    } else {
        val.className = "error-input";
        textError.style.display = "block";
    }
};

function nameValid() {
    var reg = new RegExp("^.*[^A-zА-яЁё].*$");
    var val = document.getElementById("name");
    var textError = document.getElementById("name-error");
    if (!reg.test(val.value) && (val.value.length > 1)) {
        val.className = "valid-input";
        textError.style.display = "none";
    } else {
        val.className = "error-input";
        textError.style.display = "block";
    }
};

function emailValid() {
    var reg=/[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i;
    var regRu = /[0-9a-z_]+@[0-9a-z_]+\.[ru]/i;
    var val = document.getElementById("email");
    var textError = document.getElementById("email-error");
    var textErrorRu = document.getElementById("email-error-ru");
    if (reg.test(val.value) ) {
        if (regRu.test(val.value)) {
            val.className = "error-input";
            textError.style.display = "none";
            textErrorRu.style.display = "block";
        }else {
            val.className = "valid-input";
            textError.style.display = "none";
            textErrorRu.style.display = "none";
        }
    } else {
        val.className = "error-input";
        textError.style.display = "block";
    }

    if (regRu.test(val.value)) {
        val.className = "error-input";
        textErrorRu.style.display = "block";
    }
};

function telValid() {
    var reg=/(?:\w)(?:(?:(?:(?:\+?3)?8\W{0,5})?0\W{0,5})?[34569]\s?\d[^\w,;(\+]{0,5})?\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d(?!(\W?\d))/;
    var regNaN = new RegExp("^.*[^A-zА-яЁё].*$");
    var val = document.getElementById("tel");
    var textError = document.getElementById("tel-error");
    if ( (reg.test(val.value)) && (val.value.length == 13)) {
        val.className = "valid-input";
        textError.style.display = "none";
    } else {
        val.className = "error-input";
        textError.style.display = "block";
    }
};

function loginValid() {
    var val = document.getElementById("username");
    var textError = document.getElementById("username-error");
    if (val.value.length > 3) {
        val.className = "valid-input";
        textError.style.display = "none";
    } else {
        val.className = "error-input";
        textError.style.display = "block";
    }
};

function passValid() {
    var val = document.getElementById("password");
    var val2 = document.getElementById("password_2");
    var textError = document.getElementById("password-error");
    if (val.value == val2.value) {
        val.className = "valid-input";
        val2.className = "valid-input";
        textError.style.display = "none";
    } else {
        val.className = "error-input";
        val2.className = "error-input";
        textError.style.display = "block";
    }
};

function capchaValid() {
    var reg = new RegExp("^.*[^A-zА-яЁё].*$");
    var surname = document.getElementById("surname");
    var name = document.getElementById("name");
    var email = document.getElementById("email");
    var tel = document.getElementById("tel");
    var username = document.getElementById("username");
    var val = document.getElementById("capcha");
    var pass = document.getElementById("password");
    var textError = document.getElementById("capcha-error");
    var but = document.getElementById("button-valid");
    var butDis = document.getElementById("button-dis");
    if (reg.test(val.value)) {
        val.className = "valid-input";
        textError.style.display = "none";
        if( (surname.className == "valid-input") && (name.className == "valid-input") && (email.className == "valid-input") && (tel.className == "valid-input") && (pass.className == "valid-input") && (username.className == "valid-input")){
            but.style.display = "block";
            butDis.style.display = "none";
        }
    } else {
        val.className = "error-input";
        textError.style.display = "block";
    }
};

function progresBar() {
    var loader = document.getElementById('myLoader');
    var container = document.getElementById('container');

    container.style.opacity = 0.6;
    loader.style.display = "block";

}

function myModal() {
    var modal = document.getElementsByClassName('modal');
    var loader = document.getElementsByClassName('myLoader');
    var span = document.getElementsByClassName("close")[0];
    var container = document.getElementById('container');

    loader.style.display = "none";
    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
        container.style.opacity = 1;
    };

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

