<?php include("doctype.php");
include ('stat.php');
include ('layouts/header.php') ?>

<div class="header_bg"><!-- start header -->
	<div class="container-fluid">
        <div style="background-color: #F0F7E8" class="header row">
		<nav class="navbar" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Переключити навійгаацію</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="index.php"></a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-left: 250px;">
		      <ul class="menu nav navbar-nav ">
                  <li><a href="index.php"><?php echo $row['title_button']; ?></a></li>
                  <li><a href="feature.php"><?php echo  $row['news_button'] ?> </a></li>
                  <li><a href="blog.php"><?php echo  $row['ir_button'] ?></a></li>
                  <li class="active"><a href="about.php"><?php echo  $row['about_button'] ?></a></li>
                  <li><a href="contact.php"><?php echo  $row['contact_button'] ?></a></li>
		      </ul>
<!--                <form class="navbar-form navbar-right" role="search">-->
<!--                    <div class="form-group my_search">-->
<!--                        <input type="text" class="form-control" placeholder="Пошук"><button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>-->
<!--                    </div>-->
<!--                </form>-->
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		</div>
	</div>
</div>
    <style>
        .center{
            background: beige;
        }
    </style>
<div class="main" style="background: #117a8b"><!-- start main -->
<div class="container center">
	<div class="row about"><!-- start about -->
		<div class="col-md-5 img_style">
			<img src="images/book.png" alt=""  class="img-responsive" style="text-align: left;"/>
		</div>
        <h3><strong>Ласкаво просимо!</strong></h3>
        <p>Ви увішли до сайту "Національний реєстр електронних інформаційних ресурсів". Цей сайт містить інтерфейс для доступу до ресурсів підприємств, установ, структур що їх надали.</p>
        <h3><strong>Інформація для власників ресурсів</strong></h3>
        <p>Якщо ви є власником ресурсу та маєте бажання занести цей ресурс у Національний реєстр електронних інформаційних ресурсів - будь ласка, скористайтесь інструкцією на цій сторінці.</p>
        <br>
        <h3><strong>Реєстр зареєстрованих ресурсів (Всього: 108, показано: 10)</strong></h3>
	</div><!-- end about -->
</div>
</div>
<?php include ("layouts/footer.php");?>