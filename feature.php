<?php include ('doctype.php');
include ('stat.php');
include ("layouts/header.php");?>

<div class="header_bg"><!-- start header -->
    <div class="container-fluid">
        <div style="background-color: #F0F7E8;" class="header row">
            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Переключити навігацію</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-left: 250px;">
                        <ul class="menu nav navbar-nav ">
                            <li><a href="index.php"><?php echo $row['title_button']; ?></a></li>
                            <li class="active"><a href="feature.php"><?php echo  $row['news_button'] ?> </a></li>
                            <li><a href="blog.php"><?php echo  $row['ir_button'] ?></a></li>
                            <li><a href="about.php"><?php echo  $row['about_button'] ?></a></li>
                            <li><a href="contact.php"><?php echo  $row['contact_button'] ?></a></li>
                        </ul>
<!--                        <form class="navbar-form navbar-right" role="search">-->
<!--                            <div class="form-group my_search">-->
<!--                                <input type="text" class="form-control" placeholder="Пошук"><button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>-->
<!--                            </div>-->
<!--                        </form>-->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>

    </div>
</div>
<style>
    .content{
        background: white;
    }
    .center{
        background:  beige;
    }
    .title {
        /*text-shadow: 1px 1px 1px black;*/
        color: white;
        width: 300px;
        padding-bottom: 4px;
        margin-left: 750px;
        padding-left: 40px;
        border-radius: 5px;
        background: #2e53ff;
        background: -moz-linear-gradient(top, rgb(46, 83, 255) 0%, rgba(32,51,32,1) 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgb(46, 83, 255)), color-stop(100%,rgba(32,51,32,1)));
        background: -webkit-linear-gradient(top, rgb(46, 83, 255) 0%,rgba(32,51,32,1) 100%);
        background: -o-linear-gradient(top, rgb(46, 83, 255) 0%,rgba(32,51,32,1) 100%);
        background: -ms-linear-gradient(top, rgb(46, 83, 255) 0%,rgba(32,51,32,1) 100%);
        background: linear-gradient(to bottom, rgb(46, 83, 255) 0%,rgba(32,51,32,1) 100%);

    }
    .news {
        font-family: Verdana, sans-serif;
        font-size: 17px;
        color: black;
        text-align: justify;
        background: beige;
    }
    .news_title{
        font-family: Verdana, sans-serif;
        font-size: 22px;
        font-weight: 600;
        padding-top: 10px;
    }


</style>
<div class="main content" style="background: #117a8b"><!-- start main -->
<div class="container center ">
	<div class="features "><!-- start feature -->
		<div class="new_text">
            <div class="row">
                <div class="col-lg-12">
                 <h1 style="text-align: center; padding-bottom:25px; font-weight: 600; text-decoration: underline;">Оновлення інформаційного реєстру:</h1>
                </div>
            </div>
            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
                <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.<br>
                    Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.
                    Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.
                    Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.
                    Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.<br>
                </div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст..</div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. </div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. </div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,.</div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,</div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,</div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,</div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,</div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>

            <div class="news_title"><a href="#">Проблемы с авторизацией из-за нового релиза API от Варгейминга.</a></div>
            <div class="news">Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney,</div>
            <div style=" border-bottom: dotted 1px; padding-bottom: 10px;"><h4 class="title"><strong>Додано:</strong> <i>04.09.2015 19:53</i></h4></div>
           	</div><!-- end feature -->
</div>
</div>
<?php include ("layouts/footer.php")?>