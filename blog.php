<?php include("doctype.php");
include ('stat.php');
include ('layouts/header.php'); ?>

<div class="header_bg"><!-- start header -->
	<div class="container-fluid">
        <div style="background-color: #F0F7E8" class="header row">
		<nav class="navbar" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Переключити навігацію</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="index.php"></a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-left: 250px;">
		      <ul class="menu nav navbar-nav ">
                  <li><a href="index.php"><?php echo $row['title_button']; ?></a></li>
                  <li><a href="feature.php"><?php echo  $row['news_button'] ?> </a></li>
                  <li class="active"><a href="blog.php"><?php echo  $row['ir_button'] ?></a></li>
                  <li><a href="about.php"><?php echo  $row['about_button'] ?></a></li>
                  <li><a href="contact.php"><?php echo  $row['contact_button'] ?></a></li>
		      </ul>
<!--                <form class="navbar-form navbar-right" role="search">-->
<!--                    <div class="form-group my_search">-->
<!--                        <input type="text" class="form-control" placeholder="Пошук"><button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>-->
<!--                    </div>-->
<!--                </form>-->
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		</div>
	</div>
</div>
    <style>
        .center{
            background: beige;
        }
    </style>
<div class="main" style="background: #117a8b"><!-- start main -->
<div class="container center">
	<div class="blog"><!-- start blog -->
		<div class="row">
            <h1 style="text-decoration: underline;"><strong>Для реєстрації ресурсу заповніть усі поля:</strong></h1>
            <div>
                <form method="POST" action="">
                    <table>
                        <tr>
                            <td>
                                <h3>Введіть назву ресурсу :</h3>
                                <input type="name" name="title" class="login-input-midle" placeholder=" " autofocus>
                                <h3>URL - посилання на сайт :</h3>
                                <input type="name" name="www_ir" class="login-input-midle" placeholder="" autofocus>
                                <h3>Дата публікації (у форматі: рік-місяць-день час:мин:сек)</h3>
                                <input type="datetime-local" id="localdate" name="datare"/>
                                <h3>Виберіть категорію ресурсу :</h3>
                                <br>
                                <select name='title_classif' class="login-input-midle" >
                                    <option>Зробити вивід категорій :</option>
                                </select>
                                <h3>Начальник установи :</h3>
                                <input type="name" name="owner_ir" class="login-input-midle" placeholder=" " autofocus>
                            </td>
                            <td style="padding-left: 130px;">
                                <h3>E-mail :</h3>
                                <input type="email" name="email_ir" class="login-input-midle" placeholder=" " autofocus>
                                <h3>Організація-власник ресурсу :</h3>
                                <input type="name" name="owners" class="login-input-midle" placeholder=" " autofocus>
                                <h3>Телефон :</h3>
                                <input type="name" name="phone_number" class="login-input-midle" placeholder=" " autofocus>
                                <h3>Регіон :</h3>
                                <input type="name" name="region" class="login-input-midle" placeholder=" " autofocus>
                                <h3>Адреса :</h3>
                                <input type="name" name="mithe_znahod" class="login-input-midle" placeholder=" " autofocus>
                            </td>
                        </tr></table>
                    <div class="spoiler_body"> <h3>UA - SEO Ключові слова :</h3>
                        <textarea name="key_words" class="login-input-midle" style='width: 100%; height:100px;'></textarea></div>
                    <div class="spoiler_body">
                        <h3>Опис ресурсу </h3>
                        <textarea name="description_ir" id="ck-editor-text" >Текст...</textarea></div>
                </form>
                <style>
                    .login-submit{
                        margin-top: 30px;
                        margin-left: 250px;
                        background: #326F95;
                        color: #ffffff;
                        line-height: 10px;
                        padding: 9px 28px;
                        text-transform: uppercase;
                        border-radius: 10px;
                        outline: none;
                        -webkit-transition: all 0.3s ease-in-out;
                        -moz-transition: all 0.3s ease-in-out;
                        -o-transition: all 0.3s ease-in-out;
                        transition: all 0.3s ease-in-out;
                    }
                </style>
                <form>
                    <input type="button" value="Не зберігати"  class="login-submit"  onClick='location.href="index.php"'>
                    <input type="submit" value="Зберегти" class="login-submit">
                </form>
            </div>
			<div class="clearfix"></div>
		</div>
	</div><!-- end blog -->
</div>
</div>
<?php include ("layouts/footer.php");?>